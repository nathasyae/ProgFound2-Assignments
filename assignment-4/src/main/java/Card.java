import javax.swing.*;

public class Card extends JButton{
    private int id;
    private ImageIcon face;
    private ImageIcon back;
    private ImageIcon terlihat;
    private boolean isClicked;

    /**
     * Card Constructor
     */

    public Card(int id, ImageIcon face, ImageIcon back) {
        super();
        this.id = id;
        //System.out.println("idnye "+this.id);
        this.isClicked = false;
        this.face = face;
        this.back = back;
        this.terlihat = this.back;
        setIcon(this.terlihat);

    }

    /**setter getter
     * untuk mendapatkan info & mengubah info card
     */
    public int getId() {
        return this.id;
    }

    public ImageIcon getTerlihat() {
        return this.terlihat;
    }

    public void setTerlihat(ImageIcon terlihat) {
        this.terlihat = terlihat;
    }

    public boolean isClicked() {
        return this.isClicked;
    }

    public void setClicked(boolean clicked) {
        this.isClicked = clicked;
    }

    public ImageIcon getFace() {
        return this.face;
    }

    public void setFace(ImageIcon face) {
        this.face = face;
    }

    public ImageIcon getBack() {
        return this.back;
    }

    public void setBack(ImageIcon back) {
        this.back = back;
    }






}
