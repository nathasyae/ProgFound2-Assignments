import javax.swing.*;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;


public class Board extends JFrame{
    /**
     * Instance Variable
     */
    private ArrayList<Card> cards = new ArrayList<Card>();
    private ArrayList<Card> terbuka = new ArrayList<Card>();
    private int ygKebuka=0;
    private int selectedPair=0;
    private Timer t1;
    private Timer t2;
    private Timer t3;
    private JLabel labelCounter;
    private JLabel labelNumber;
    private JLabel labelName;
    private JLabel labelPesan;
    private JButton buttonRestart;
    private JButton buttonExit;

    /**
     * Board Constructor
     */
    public Board(String name){
        //Tampilan dasar
        setTitle("Match-Pair Game");
        setBackground(Color.LIGHT_GRAY);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        setPreferredSize(new Dimension(800,768));
        setLocation(100, 0);

        //Membuat panel yang akan berisi images
        JPanel picPanel = new JPanel();
        picPanel.setMaximumSize(new Dimension(600,768));
        picPanel.setLayout(new GridLayout(6,6));
        for (int i=1; i<19; i++){
            String filename1 = null;
            String filename2 = null;
            try {
                filename1 = i+".jpg";
                filename2 = "back.jpg";
            } catch (Exception e) {
                System.out.println("Images location is not valid");
            }
            Card c = new Card(i,resizeIcon(new ImageIcon(filename1)),resizeIcon(new ImageIcon(filename2)));
            Card c2 = new Card(i,resizeIcon(new ImageIcon(filename1)),resizeIcon(new ImageIcon(filename2)));;
            c.addActionListener(e->clickAct(c));
            c2.addActionListener(e->clickAct(c2));
            cards.add(c);
            cards.add(c2);
        }

        //mengacak kartu
        Collections.shuffle(cards);

        //memasukkan ke panel
        for (Card i: cards){
            picPanel.add(i);
        }

        //Membuat panel info
        JPanel info = new JPanel(new GridLayout(3,3));
        info.setMaximumSize(new Dimension(100,770));

        JButton buttonExit = new JButton("Exit ah, pusing");
        JButton buttonRestart = new JButton("Mari memulai hidup baru");
        buttonRestart.addActionListener(actionPerformed -> restart());
        buttonExit.addActionListener(actionPerformed -> exit());

        labelName = new JLabel(name);
        labelPesan = new JLabel("Selamat bermain, ");
        labelCounter = new JLabel("Sudah berpasangan: ");
        labelNumber = new JLabel(String.valueOf(selectedPair));
        labelName.setHorizontalAlignment(SwingConstants.LEFT);
        labelPesan.setHorizontalAlignment(SwingConstants.RIGHT);
        labelCounter.setHorizontalAlignment(SwingConstants.RIGHT);
        labelNumber.setHorizontalAlignment(SwingConstants.LEFT);

        //Meng-add ke panel
        info.add(buttonRestart);
        info.add(buttonExit);
        info.add(labelPesan);
        info.add(labelName);
        info.add(labelCounter);
        info.add(labelNumber);


        //Meng-add ke board
        add(picPanel, BorderLayout.CENTER);
        add(info, BorderLayout.EAST);

        //pack
        pack();
        setVisible(true);
    }

    /**
     * Method untuk mengatur ukuran icon
     */
    public static ImageIcon resizeIcon(ImageIcon icon){
        Image im = icon.getImage();
        Image resizedim = im.getScaledInstance(100,128,java.awt.Image.SCALE_SMOOTH);
        return new ImageIcon(resizedim);
    }

    /**
     * Method untuk mengecek apakah sudah menang
     */
    public boolean isWon(){
        if (selectedPair==18){
            return true;
        } else { return false;}
    }

    /**
     * Method untuk menghilangkan pasangan kartu yang sudah menemukan pasangan
     */
    public void remove(){
        for (Card c: terbuka){
            c.setEnabled(false);
        }
        selectedPair++;
    }

    /**
     * Mengecek gambarnya sudah sama belum
     */
    public boolean samaGa(Card a, Card b){
        if (a.getId()==b.getId()){
            return true;
        } else { return false; }
    }

    /**
     * method saat diklik
     */
    public void clickAct(Card c){
        if (c.isClicked()==false) {
            c.setClicked(true);
            terbuka.add(c);

            if (ygKebuka == 0) {
                flip(c);
                ygKebuka++;
            }
            else if (ygKebuka == 1) {
                flip(c);
                if (samaGa(terbuka.get(0),terbuka.get(1))) {
                    remove();
                    labelNumber.setText(String.valueOf(selectedPair));
                    if (isWon()==true){
                        JOptionPane.showMessageDialog(this, "You win! So cool bro, good game");
                    }
                } else {
                    Card a = terbuka.get(0);
                    Card b = terbuka.get(1);
                    t1 = new Timer(1000, actionPerformed -> flip(a));
                    t2 = new Timer(1000, actionPerformed -> flip(b));
                    t1.setRepeats(false);
                    t2.setRepeats(false);
                    t1.start();
                    t2.start();
                }
                terbuka.clear();
                ygKebuka = 0;
            }
        }
    }


    /**
     * Membalik kartu
     */
    public void flip(Card c){
        if (c.getTerlihat().equals(c.getBack())){
            c.setTerlihat(c.getFace());
            c.setIcon(c.getTerlihat());

        } else if (c.getTerlihat().equals(c.getFace())){
            c.setTerlihat(c.getBack());
            c.setIcon(c.getTerlihat());
            c.setClicked(false);
        }
    }

    /**
     * Mengulang permainan
     */
    public void restart(){
        for(Card c: cards){
            c.setClicked(false);
            c.setIcon(c.getBack());
            c.setEnabled(true);
        }
        selectedPair=0;
        labelNumber.setText(String.valueOf(selectedPair));
        ygKebuka=0;
        terbuka.clear();
        Collections.shuffle(cards);
    }

    /**
     * Keluar dari permainan
     */
    private void exit() {
        System.exit(0);
    }
}
