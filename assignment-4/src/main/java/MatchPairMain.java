import javax.swing.*;

public class MatchPairMain {
    /**
     * MAIN
     */
    public static void main(String[] args){
        String name;
        name = JOptionPane.showInputDialog(null,"Nenek saya pernah bilang\nTak kenal maka tak sayang\nNah, makanya kenalan yuk\nTulis nama kamu disini ya");
        if (name!=null) {
            Board theB = new Board(name);
        } else {
            System.exit(0);
        }
    }
}

