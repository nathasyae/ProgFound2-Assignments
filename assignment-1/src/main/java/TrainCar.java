/**
 Nathasya Eliora K.
 1706979404
 DDP 2 Kelas B
 TP 1
 */

public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms
    private WildCat cat;
    private TrainCar next;
    private double totalWeight;
    private double totalBMI = 0.0;


    public TrainCar(WildCat cat) {
        this.cat = cat;
        this.next = null;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
        this.next = next;
    }

    public double computeTotalWeight() {
        //double totalWeight = 0;
        if (this.next==null) {
            this.totalWeight = EMPTY_WEIGHT+cat.weight;
        }
        else{
            this.totalWeight = EMPTY_WEIGHT+cat.weight+this.next.computeTotalWeight();
        }
        return this.totalWeight;
    }

    public double computeTotalMassIndex() {
        //double totalBMI = 0;
        if (this.next==null){
            this.totalBMI = this.cat.computeMassIndex();
        } else {
            this.totalBMI = this.cat.computeMassIndex() + this.next.computeTotalMassIndex();
        }
        return this.totalBMI;
    }

    public void printCar() {
        /**if (next==null){
            ngeprint = cat.name;
        }
        else{
           ngeprint = cat.name+"--"+next.printCar();

        }
         */
        if (this.next==null){
            System.out.print("("+this.cat.name+")");
        }
        else {
            System.out.print("("+this.cat.name+")" + "--");
            this.next.printCar();
        }
    }
}
