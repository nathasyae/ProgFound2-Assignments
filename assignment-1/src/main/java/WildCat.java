/**
 Nathasya Eliora K.
 1706979404
 DDP 2 Kelas B
 TP 1
 */

public class WildCat {
    String name;
    double weight; // In kilograms
    double length; // In centimeters
    double BMI;

    public WildCat(String name, double weight, double length) {
        this.name = name;
        this.weight = weight;
        this.length = length;
    }

    public double computeMassIndex() {
        this.BMI = this.weight / (this.length*this.length/10000);
        return this.BMI;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }
}
