/**
 Nathasya Eliora K.
 1706979404
 DDP 2 Kelas B
 TP 1
 */

import sun.awt.ConstrainableGraphics;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms
    public static ArrayList<WildCat> kucing2ku = new ArrayList<WildCat>();
    public static ArrayList<TrainCar> kereta = new ArrayList<TrainCar>();


    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        //String[] dataSplit = new Arrays[3];

        //inputan
        int jumlah = Integer.parseInt(input.nextLine());
   //   TrainCar gerbongs;

        for (int i=0; i<jumlah; i++) {
            String data = input.nextLine();
            String[] dataSplit = data.split(",");
            String name = dataSplit[0];
            int weight = Integer.parseInt(dataSplit[1]);
            int length = Integer.parseInt(dataSplit[2]);
            WildCat thecat = new WildCat(name, weight, length);
            kucing2ku.add(thecat);

            if (i == 0) {
                kereta.add(new TrainCar(kucing2ku.get(i)));
                if (jumlah == 1) {
                    berangkat(kereta.get(i));
                }
                else if(kereta.get(kereta.size()-1).computeTotalWeight() >= THRESHOLD){
                        berangkat(kereta.get(kereta.size() - 1));
                        kereta.clear();
                }
            } else {
                if (kereta.size()==0){
                    kereta.add(new TrainCar(kucing2ku.get(i)));
                    if (i==jumlah-1){
                        berangkat(kereta.get(kereta.size()-1));
                    }
                }
                else {
                    kereta.add(new TrainCar(kucing2ku.get(i), kereta.get(kereta.size() - 1)));
                    if ((kereta.get(kereta.size() - 1).computeTotalWeight() >= THRESHOLD) || (i == jumlah - 1)) {
                        berangkat(kereta.get(kereta.size() - 1));
                        kereta.clear();
                        //kereta.add(new TrainCar(kucing2ku.get(i)));
                    }
                }
            }
        }
    }

    private static void berangkat(TrainCar gerbongs) {
        System.out.println("The train departs to Javari Park");

        System.out.print("[LOCO]<--");
        gerbongs.printCar();
        System.out.println();

        //keterangan
        double BMInya = gerbongs.computeTotalMassIndex();
        //System.out.println(BMInya);
        String keadaan = "";
        double BMIfix = BMInya/kereta.size();
        System.out.printf("Average mass index of all cats: %.2f \n",(BMIfix));
        if ((BMIfix)>=30.0){
            keadaan = "*obese*";
        } else if ((BMIfix)>=25){
            keadaan = "*overweight*";
        } else if ((BMIfix)>=18.5){
            keadaan = "*normal*";
        } else{
            keadaan = "*underweight*";
        }
        System.out.println("In average, the cats in the train are "+keadaan);}

}

