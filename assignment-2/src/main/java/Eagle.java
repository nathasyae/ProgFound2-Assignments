public class Eagle extends Animals {
    public Eagle(String name, int length) {
        super(name, length);
        isPet = false;
        spesies = "eagle";
    }

    public static void fly(){
        System.out.println("Kwaakk....");
    }

    /**
        //fly
        public void fly(){
            System.out.println(this.name + "makes a voice: Kwaakk....");
    }
     */
}