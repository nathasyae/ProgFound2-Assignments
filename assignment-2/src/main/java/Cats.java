//membuat kelas kucing

import java.util.Random;

public class Cats extends Animals {


    public Cats(String name, int length) {
        super(name, length);
        isPet = true;
        spesies = "cat";

    }

    public static void brush(){
        System.out.println("Nyaaan...");
    }

    public static void cuddle(){
        Random rndm = new Random();
        String[] sounds = new String[]{"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};
        System.out.println(sounds[rndm.nextInt(sounds.length)]);
    }

    /**
        public void brush(){
            System.out.println(this.name + "makes a voice: Nyaaan...");
        }

        public void cuddle(){
            String[] sounds = new String[]{"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};
            String bunyi = sounds[rndm.nextInt(sounds.length)];
            System.out.println(this.name + "makes a voice: " + bunyi );
        }
     */
    }
