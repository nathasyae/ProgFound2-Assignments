import java.util.ArrayList;
import java.util.Random;

import java.util.Random;

public class Animals {
    protected String spesies;
    protected String name;
    protected int length;
    protected String cageInfo;
    protected static boolean isPet = true;

    //setter getter

    public String getSpesies() {
        return spesies;
    }

    public void setSpesies(String spesies) {
        this.spesies = spesies;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String getCageInfo() {
        return cageInfo;
    }

    public void setCageInfo(String cageInfo) {
        this.cageInfo = cageInfo;
    }

    public boolean isPet() {
        return isPet;
    }

    public void setPet(boolean pet) {
        isPet = pet;
    }

    public Animals() {
    }

    public Animals(String name, int length) {
        this.name = name;
        this.length = length;
    }
}
