import org.omg.Messaging.SYNC_WITH_TRANSPORT;

import java.util.ArrayList;
import java.util.Scanner;

public class Visiting {
    static int tujuan;
    static String namaHewan;
    static int command;

    Scanner input = new Scanner (System.in);

    public static void visit(ArrayList<Animals> dataAnimals) {
        Scanner input = new Scanner(System.in);
        boolean isDone = false;
        while (!isDone) {
            System.out.println("Which animal you want to visit?\n" +
                    "(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit)");
            tujuan = input.nextInt();
            boolean ada = false;

            switch (tujuan) {
                //cat
                case 1:
                    System.out.print("Mention the name of cat you want to visit: ");
                    namaHewan = input.next();
                    for (Animals i : dataAnimals) {
                        if (i.getName().equals(namaHewan)) {
                            ada = true;
                            System.out.printf("You are visiting %s (%s) now, what would you like to do?\n" +
                                    "1: Brush the fur 2: Cuddle\n ", namaHewan, i.getSpesies());
                            command = input.nextInt();

                            switch (command) {
                                //brush
                                case 1:
                                    bunyi(namaHewan);
                                    Cats.brush();
                                    break;
                                //cuddle
                                case 2:
                                    bunyi(namaHewan);
                                    Cats.cuddle();
                                    break;
                                //do nothin
                                default:
                                    nothin();
                            }
                        }
                    }
                    if (ada == false) {
                        System.out.print("There is no cat with that name! ");
                    }
                    System.out.println("Back to the office!\n");
                    break;

                //eagle
                case 2:
                    System.out.print("Mention the name of eagle you want to visit: ");
                    namaHewan = input.next();
                    for (Animals i : dataAnimals) {
                        if (i.getName().equals(namaHewan)) {
                            ada = true;
                            System.out.printf("You are visiting %s (%s) now, what would you like to do?\n" +
                                    "1: Order to fly", namaHewan, i.getSpesies());
                            command = input.nextInt();

                            switch (command) {
                                //fly
                                case 1:
                                    bunyi(namaHewan);
                                    Eagle.fly();
                                    System.out.println("You hurt!");
                                    break;
                                //do nothin
                                default:
                                    nothin();
                            }
                        }
                    }
                    if (ada == false) {
                        System.out.print("There is no eagle with that name! ");
                    }
                    System.out.println("Back to the office!\n");
                    break;

                //hamster
                case 3:
                    System.out.print("Mention the name of hamster you want to visit: ");
                    namaHewan = input.next();
                    for (Animals i : dataAnimals) {
                        if (i.getName().equals(namaHewan)) {
                            ada = true;
                            System.out.printf("You are visiting %s (%s) now, what would you like to do?\n" +
                                    "1: See it gnawing 2: Order to run in the hamster wheel", namaHewan, i.getSpesies());
                            command = input.nextInt();

                            switch (command) {
                                //gnaw
                                case 1:
                                    bunyi(namaHewan);
                                    Hamsters.gnaw();
                                    break;
                                //run the wheel
                                case 2:
                                    bunyi(namaHewan);
                                    Hamsters.runthewheel();
                                    break;
                                //do nothin
                                default:
                                    nothin();
                            }
                        }
                    }
                    if (ada == false) {
                        System.out.print("There is no eagle with that name! ");
                    }
                    System.out.println("Back to the office!\n");
                    break;

                //parrot
                case 4:
                    System.out.print("Mention the name of parrot you want to visit: ");
                    namaHewan = input.next();
                    for (Animals i : dataAnimals) {
                        if (i.getName().equals(namaHewan)) {
                            ada = true;
                            System.out.printf("You are visiting %s (%s) now, what would you like to do?\n" +
                                    "1: Order to fly 2: Do conversation", namaHewan, i.getSpesies());
                            command = input.nextInt();
                            input.nextLine();

                            switch (command) {
                                //fly
                                case 1:
                                    System.out.printf("Parrot %s flies!", namaHewan);
                                    bunyi(namaHewan);
                                    Parrots.fly();
                                    break;
                                //do convo
                                case 2:
                                    System.out.println("You say: ");
                                    String talk = input.nextLine();
                                    Parrots.convo(talk);
                                    break;
                                //do nothin
                                default:
                                    nothin();
                            }
                        }
                    }
                    if (ada == false) {
                        System.out.print("There is no eagle with that name! ");
                    }
                    System.out.println("Back to the office!\n");
                    break;

                //lion
                case 5:
                    System.out.print("Mention the name of lion you want to visit: ");
                    namaHewan = input.next();
                    for (Animals i : dataAnimals) {
                        if (i.getName().equals(namaHewan)) {
                            ada = true;
                            System.out.printf("You are visiting %s (%s) now, what would you like to do?\n" +
                                    "1: See it hunting 2: Brush the mane 3: Disturb it", namaHewan, i.getSpesies());
                            command = input.nextInt();

                            switch (command) {
                                //hunting
                                case 1:
                                    System.out.println("Lion is hunting..");
                                    bunyi(namaHewan);
                                    Lion.hunting();
                                    break;
                                //brush
                                case 2:
                                    System.out.println("Clean the lion’s mane..");
                                    bunyi(namaHewan);
                                    Lion.brush();
                                    break;
                                //disturb
                                case 3:
                                    bunyi(namaHewan);
                                    Lion.disturb();
                                    break;
                                //do nothin
                                default:
                                    nothin();
                            }
                        }
                    }
                    if (ada == false) {
                        System.out.print("There is no eagle with that name! ");
                    }
                    System.out.println("Back to the office!\n");
                    break;

                //Exit
                case 99:
                    isDone=true;
                    break;

                default:
                    System.out.println("Back to the office!\n");
                    break;
            }
        }
    }

    public static void bunyi(String namanya){
        System.out.printf("%s makes a voice: ",namanya);
    }

    public static void nothin(){
        System.out.print("You do nothing... ");
    }

    public static void says(String namanya){
        System.out.printf("%s says: ",namanya);
    }
}
