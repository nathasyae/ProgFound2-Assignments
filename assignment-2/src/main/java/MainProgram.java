import java.util.ArrayList;
import java.util.Scanner;

import static java.lang.Integer.parseInt;

    public class MainProgram {
        static ArrayList<Animals> dataAnimals = new ArrayList<Animals>();
        static ArrayList<Cages> dataCages = new ArrayList<Cages>();
        static ArrayList<Animals> listCat = new ArrayList<>();
        static ArrayList<Animals> listEagle = new ArrayList<>();
        static ArrayList<Animals> listHamster = new ArrayList<>();
        static ArrayList<Animals> listParrot = new ArrayList<>();
        static ArrayList<Animals> listLion = new ArrayList<>();


        public static void main(String[] args) {
            Scanner input = new Scanner(System.in);

            System.out.println("Welcome to Javari Park!");
            System.out.println("Input the number of animals");

            //urusan input

            //cat
            System.out.print("cat: ");
            int jumlahCat = input.nextInt();
            if (jumlahCat != 0) {
                System.out.println("Provide the information of cat(s): ");
                String dataCat = input.next();
                String[] dataCatSplit = dataCat.split(",");
                String[] dataPerCat = new String[jumlahCat];
                for (int i = 0; i < jumlahCat; i++) {
                    dataPerCat[i] = dataCatSplit[i];
                }
                String[][] dataFinalCat = new String[jumlahCat][2];
                for (String i : dataPerCat) {
                    String nm = i.split("\\|")[0];
                    int pjg = Integer.parseInt(i.split("\\|")[1]);
                    dataAnimals.add(new Cats(nm, pjg));
                    listCat.add(new Cats(nm, pjg));
                }
            }

            //lion
            System.out.print("lion: ");
            int jumlahLion = input.nextInt();
            if (jumlahLion != 0) {
                System.out.println("Provide the information of lion(s): ");
                String dataLion = input.next();
                String[] dataLionSplit = dataLion.split(",");
                String[] dataPerLion = new String[jumlahLion];
                for (int i = 0; i < jumlahLion; i++) {
                    dataPerLion[i] = dataLionSplit[i];
                }
                String[][] dataFinalLion = new String[jumlahLion][2];
                for (String i : dataPerLion) {
                    String nm = i.split("\\|")[0];
                    int pjg = Integer.parseInt(i.split("\\|")[1]);
                    dataAnimals.add(new Lion(nm, pjg));
                    listLion.add(new Lion(nm, pjg));
                }
            }

            //eagle
            System.out.print("eagle: ");
            int jumlahEagle = input.nextInt();
            if (jumlahEagle != 0) {
                System.out.println("Provide the information of eagle(s): ");
                String dataEagle = input.next();
                String[] dataEagleSplit = dataEagle.split(",");
                String[] dataPerEagle = new String[jumlahEagle];
                for (int i = 0; i < jumlahEagle; i++) {
                    dataPerEagle[i] = dataEagleSplit[i];
                }
                String[][] dataFinalEagle = new String[jumlahEagle][2];
                for (String i : dataPerEagle) {
                    String nm = i.split("\\|")[0];
                    int pjg = Integer.parseInt(i.split("\\|")[1]);
                    dataAnimals.add(new Eagle(nm, pjg));
                    listEagle.add(new Eagle(nm, pjg));
                }
            }

            //parrot
            System.out.print("parrot: ");
            int jumlahParrot = input.nextInt();
            if (jumlahParrot != 0) {
                System.out.println("Provide the information of parrot(s): ");
                String dataParrot = input.next();
                String[] dataParrotSplit = dataParrot.split(",");
                String[] dataPerParrot = new String[jumlahParrot];
                for (int i = 0; i < jumlahParrot; i++) {
                    dataPerParrot[i] = dataParrotSplit[i];
                }
                String[][] dataFinalParrot = new String[jumlahParrot][2];
                for (String i : dataPerParrot) {
                    String nm = i.split("\\|")[0];
                    int pjg = Integer.parseInt(i.split("\\|")[1]);
                    dataAnimals.add(new Parrots(nm, pjg));
                    listParrot.add(new Parrots(nm, pjg));
                }
            }

            //hamster
            System.out.print("hamster: ");
            int jumlahHamster = input.nextInt();
            if (jumlahHamster != 0) {
                System.out.println("Provide the information of hamster(s): ");
                String dataHamster = input.next();
                String[] dataHamsterSplit = dataHamster.split(",");
                String[] dataPerHamster = new String[jumlahHamster];
                for (int i = 0; i < jumlahHamster; i++) {
                    dataPerHamster[i] = dataHamsterSplit[i];
                }
                String[][] dataFinalHamster = new String[jumlahHamster][2];
                for (String i : dataPerHamster) {
                    String nm = i.split("\\|")[0];
                    int pjg = Integer.parseInt(i.split("\\|")[1]);
                    dataAnimals.add(new Hamsters(nm, pjg));
                    listHamster.add(new Hamsters(nm, pjg));
                }
            }
            System.out.println("Animals have been successfully recorded!\n");
            System.out.println("=============================================");

            //membuat cages2
            /**for (Animals i: dataAnimals){
             dataCages.add(new Cages(i));
             }*/

            //untuk mengerrange
            if (dataAnimals.size()!=0) {
                System.out.println("Cage arrangement:");
                if (listCat.size() != 0) {
                    Arrangement.arrange(listCat);
                }
                if (listLion.size() != 0) {
                    Arrangement.arrange(listLion);
                }
                if (listEagle.size() != 0) {
                    Arrangement.arrange(listEagle);
                }
                if (listParrot.size() != 0) {
                    Arrangement.arrange(listParrot);
                }
                if (listParrot.size() != 0) {
                    Arrangement.arrange(listParrot);
                }
            }

        //informasi animals
        System.out.println("\nNUMBER OF ANIMALS:");
        System.out.println("cat:"+listCat.size());
        System.out.println("lion:"+listLion.size());
        System.out.println("parrot:"+listParrot.size());
        System.out.println("eagle:"+listEagle.size());
        System.out.println("hamster:"+listHamster.size());
        System.out.println("\n=============================================");

        //untuk action visit
        Visiting.visit(dataAnimals);

    }
}


/**
 for (int i = 0; i < jumlahCat; i++) {
 dataFinalCat[i][0] = dataPerCat[i].split("\\|")[0];
 dataFinalCat[i][1] = dataPerCat[i].split("\\|")[1];
 dataAnimals.add(new Cats(dataFinalCat[i][0], Integer.parseInt(dataFinalCat[i][1])));
 }

Simba|74,Mambo|80;
{Simba|74, Mambo|80}

dataHewan = input.next;
listData<> = detaHewan.split(",");
ArrayList<Animals> dataFix;
for (int i=0; i<listData.length; i++){
    temp<> = listData[i].split("|")
    dataFix.add(Animals(temp[0],temp[1])); //sebenernya akses subclassnya lgsg aja gmn crnya tp
        }
*/