import java.util.ArrayList;
import java.util.Scanner;

public class Arrangement {
    static ArrayList<Cages> level1 = new ArrayList<Cages>();
    static ArrayList<Cages> level2 = new ArrayList<Cages>();
    static ArrayList<Cages> level3 = new ArrayList<Cages>();
    static ArrayList<Cages> level1Final = new ArrayList<Cages>();
    static ArrayList<Cages> level2Final = new ArrayList<Cages>();
    static ArrayList<Cages> level3Final = new ArrayList<Cages>();
    static ArrayList<Cages>[] beforeAtur = new ArrayList[3];
    static ArrayList<Cages>[] afterAtur = new ArrayList[3];


    static ArrayList<Cages> listCages = new ArrayList<Cages>();

    //method untuk mengarrange segalanya
    public static void arrange(ArrayList<Animals> listhewan) {
        bikinCages(listhewan);
        masukinKeLevel(listCages);
        System.out.printf("location: %s\n", beforeAtur[0].get(0).getType());
        ngeprint(beforeAtur);
        System.out.println("\nAfter rearrangement...");
        finalArrange(beforeAtur);
        ngeprint(afterAtur);
        System.out.println();
        clearAll();
    }

    //method untuk membuat cages untuk masing2 hewan
    public static void bikinCages(ArrayList<Animals> listhewan) {
        for (Animals i : listhewan) {
            listCages.add(new Cages(i));
        }
    }

    //method untuk memasukan cages ke level 1 2 3
    public static void masukinKeLevel(ArrayList<Cages> listcages) {
        //kalo kurang dari 3 hewannya
        if (listcages.size() == 1) {
            level1.add(listcages.get(0));
        } else if (listcages.size() == 2) {
            level1.add(listcages.get(0));
            level2.add(listcages.get(1));
        } else if (listcages.size() == 3) {
            level1.add(listcages.get(0));
            level2.add(listcages.get(1));
            level3.add(listcages.get(2));
        }
        //kalo lebih dari 3
        else {
            int divider = listcages.size() / 3;
            for (int i = 0; i < listcages.size(); i++) {
                if (i < divider) {
                    level1.add(listcages.get(i));
                } else if (i < 2 * divider) {
                    level2.add(listcages.get(i));
                } else {
                    level3.add(listcages.get(i));
                }
            }
        }
        beforeAtur[0] = level1;
        beforeAtur[1] = level2;
        beforeAtur[2] = level3;
    }

    public static void printloop(ArrayList<Cages> listnya){
        for (int i = 0; i<listnya.size(); i++){
            String nm = listnya.get(i).getIsi().getName();
            int pjg = listnya.get(i).getIsi().getLength();
            String sz = listnya.get(i).getSize();
            System.out.printf("%s (%d - %s), ", nm,pjg,sz);
        }
    }
        //untuk mencetak informasi level2
        public static void ngeprint(ArrayList<Cages>[] listcages){
            System.out.print("level 3: ");
            printloop(listcages[2]);
            System.out.println();
            System.out.print("level 2: ");
            printloop(listcages[1]);
            System.out.println();
            System.out.print("level 1: ");
            printloop(listcages[0]);
            System.out.println();
            }

        //melakukan swap
        public static ArrayList<Cages> swap(ArrayList<Cages> listasal){
            ArrayList<Cages> temp = new ArrayList<Cages>();
            for (int i = listasal.size()-1; i>=0 ;i--){
                temp.add(listasal.get(i));
            }
            listasal = temp;
            return listasal;
        }
        //untuk mengearrange finalisasi
        public static void finalArrange(ArrayList[] listcages){
            level1Final = swap(level3);
            level2Final = swap(level1);
            level3Final = swap(level2);
            afterAtur[0] = level1Final;
            afterAtur[1] = level2Final;
            afterAtur[2] = level3Final;
        }

        public static void clearAll(){
            level1.clear();
            level2.clear();
            level3.clear();
            level1Final.clear();
            level2Final.clear();
            level3Final.clear();
            listCages.clear();
            for (int i=0; i<3;i++) {
                beforeAtur[i] = null;
                afterAtur[i] = null;
            }
        }
}
