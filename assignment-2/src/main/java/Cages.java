public class Cages {
    //attributes
    private String type;
    private String size;
    private Animals isi;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public Animals getIsi() {
        return isi;
    }

    public void setIsi(Animals isi) {
        this.isi = isi;
    }

    public Cages(Animals hewan) {
        this.isi = hewan;

        //untuk hewan buas
        if (hewan.isPet()==false){
            type = "outdoor";
            if (isi.length<75){
                this.size = "A";
            } else if (75<isi.length && isi.length<90){
                this.size = "B";
            } else {
                this.size = "C";
            }
            hewan.setCageInfo(type+" "+this.size);
        }

        //untuk hewan peliharaan
        else {
            type = "indoor";
            if (isi.length<45){
                this.size = "A";
            } else if (45<isi.length && isi.length<60){
                this.size = "B";
            } else {
                this.size = "C";
            }
            isi.setCageInfo(type+" "+this.size);
        }
    }


}

