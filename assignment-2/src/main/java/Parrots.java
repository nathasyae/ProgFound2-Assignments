public class Parrots extends Animals {
    public Parrots(String name, int length) {
        super();
        this.name = name;
        this.length = length;
        isPet = true;
        spesies = "parrot";
    }

    public static void fly(){
        System.out.println("FLYYYY…..");
    }

    public static void convo(String talk) {
        System.out.println(talk.toUpperCase());
    }

    public static void hm(){
        System.out.println("HM?");
    }
    /**
        public void fly(){
            System.out.printf("Parrot %s flies!\n%s makes a voice: FLYYYY…..",this.name, this.name);
        }

        public void convo(String talk){
            System.out.printf("%s says: %s\n", this.name, talk.toUpperCase()
);
        }

        public void hm(){
            System.out.printf("%s says: HM?\n", this.name);
        }
     */

    }
