package javari;

import javari.animal.Animal;
import javari.animal.Aves.Aves;
import javari.animal.Aves.AvesInfo;
import javari.animal.Mammals;
import javari.animal.Mammals.MammalsInfo;
import javari.animal.Reptiles.Reptiles;
import javari.animal.Reptiles.ReptilesInfo;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;

public class ProgramUtama {
    public static ArrayList<Animal> listAnimal = new ArrayList<>();

    public static void main (String[] args){
        Scanner input = new Scanner (System.in);

        System.out.println("Welcome to Javari Park Festival - Registration Service!");

        //opening default
        System.out.print("... Opening default section database from data. ... ");
        String defaultPath = "D:\\ProgFound2-Assignments\\assignment-3\\data";
        //if (not found){
          //  System.out.println("File not found or incorrect file!");
        //}

        //minta path manual lain
        System.out.println("Please provide the source data path: ");
        String otherPath = input.nextLine();

        //cek

        //print info
        System.out.printf("Found _%d_ valid sections and _0_ invalid sections\n");
        System.out.printf("Found _%d_ valid attractions and _0_ invalid attractions\n");
        System.out.printf("Found _%d_ valid animal categories and _0_ invalid animal categories\n");
        System.out.printf("Found %d_ valid animal records and _0_ invalid animal records\n");

        //mulai
        System.out.println("Welcome to Javari Park Festival - Registration Service!");
        System.out.println("Please answer the questions by typing the number. Type # if you want to return to the previous menu");

        //looping input
        boolean flag = true;
        while (flag){
            System.out.print("Javari Park has 3 sections:\n" +
                    "1. Explore the Mammals\n" +
                    "2. World of Aves\n" +
                    "3. Reptilian Kingdom\n" +
                    "Please choose your preferred section (type the number): ");
            String perintah = input.next();
            String pilihHewan;

            switch(perintah){
                case "1":
                    MammalsInfo.chooseSpesies();
                    pilihHewan = input.next();
                    break;

                case "2":
                    AvesInfo.chooseSpesies();
                    pilihHewan = input.next();
                    break;

                case "3":
                    ReptilesInfo.chooseSpesies();
                    pilihHewan = input.next();
                    break;

                case "#":
                    flag=false;
                    break;

                default:
                    break;
            }

        }

    }


}
