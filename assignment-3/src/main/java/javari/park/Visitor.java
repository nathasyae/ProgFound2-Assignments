package javari.park;

import java.util.ArrayList;
import java.util.List;

public class Visitor implements Registration {
    private int id=0;
    private String name;
    private List<SelectedAttraction> listAttractions = new ArrayList<>();

    /**
     * Returns the unique ID that associated with visitor's registration
     * in watching an attraction.
     *
     * @return
     */
    public int getRegistrationId() {
        id += 1;
        return id;
    }

    /**
     * Returns the name of visitor that associated with the registration.
     *
     * @return
     */
    public String getVisitorName(){
        return this.name;
    }

    /**
     * Changes visitor's name in the registration.
     *
     * @param name  name of visitor
     * @return
     */
    public String setVisitorName(String name) {
        this.name = name;
    }

    /**
     * Returns the list of all attractions that will be watched by the
     * visitor.
     *
     * @return
     */
    public List<SelectedAttraction> getSelectedAttractions() {
        return this.listAttractions;
    }

    /**
     * Adds a new attraction that will be watched by the visitor.
     *
     * @param selected  the attraction
     * @return {@code true} if the attraction is successfully added into the
     *     list, {@code false} otherwise
     */

    //BELOM
    public boolean addSelectedAttraction(SelectedAttraction selected){

    }

}
