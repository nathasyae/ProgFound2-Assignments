package javari.park;

import javari.animal.Animal;

import java.util.ArrayList;
import java.util.List;

public class CircleOfFire implements SelectedAttraction {
    private String name = "Circle Of Fire";
    private static List<Animal> listPerformer = new ArrayList<>();
    public static String[] ygBisa = new String[]{"Lion","Whales","Eagle"};

    /**
     * Returns the name of attraction.
     *
     * @return
     */
    public String getName() {
        return this.name;
    }

    /**
     * Returns ths type of animal(s) that performing in this attraction.
     *
     * @return
     */
    public String getType() {
        return listPerformer.get(0).getType();
    }

    /**
     * Returns all performers of this attraction.
     *
     * @return
     */
    public List<Animal> getPerformers() {
        return this.listPerformer;
    }

    /**
     * Adds a new animal into the list of performers.
     *
     * @param performer an instance of animal
     * @return {@code true} if the animal is successfully added into list of
     *     performers, {@code false} otherwise
     */
    //BELOM
    public static boolean addPerformer(Animal performer){
            if (performer.isShowable()) {
                listPerformer.add(performer);
                return true;
            }
            return false;
        }


}
