package javari.animal.Mammals;

import javari.animal.Condition;
import javari.animal.Gender;
import javari.animal.Mammals;

public class Whale extends Mammals {
    protected boolean isPregnant;

    public Whale(Integer id, String type, String name, Gender gender, double length,
                 double weight, Condition condition, String hamilga){
        super(id,type,name,gender,length,weight, condition,hamilga);
        }

    public boolean isPregnant() {
        return isPregnant;
    }

    public void setPregnant(boolean pregnant) {
        isPregnant = pregnant;
    }

    public void chooseAttraction() {
        System.out.print("\n--Whale--\n" +
                "Attraction by Whale\n" +
                "1. Circle of Fire\n" +
                "2. Counting Masters" +
                "Please choose your preferred attractions (type the number): ");
    }
}
