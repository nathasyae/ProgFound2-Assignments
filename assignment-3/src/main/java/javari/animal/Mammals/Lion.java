package javari.animal.Mammals;

import javari.animal.Condition;
import javari.animal.Gender;
import javari.animal.Mammals;

public class Lion extends Mammals {
    protected boolean isPregnant;

    public Lion(Integer id, String type, String name, Gender gender, double length,
                double weight, Condition condition, String hamilga){
        super(id,type,name,gender,length,weight, condition,hamilga);
    }

    public boolean isPregnant() {
        return isPregnant;
    }

    public void setPregnant(boolean pregnant) {
        isPregnant = pregnant;
    }

    public boolean specficCondition(){ return !(this.isPregnant) && this.getGender().equals("male");}

    public static void chooseAttraction() {
        System.out.print("\n--Lion--\n" +
                "Attraction by Lion\n" +
                "1. Circle of Fire\n" +
                "Please choose your preferred attractions (type the number): ");
    }

    public boolean isPregnant() {
        return isPregnant;
    }

    public void setPregnant(boolean pregnant) {
        isPregnant = pregnant;
    }

    public boolean specficCondition(){ return !(this.isPregnant);}
}
