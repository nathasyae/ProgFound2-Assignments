package javari.animal.Mammals;

import javari.animal.Condition;
import javari.animal.Gender;
import javari.animal.Mammals;

public class Cat extends Mammals {
    protected boolean isPregnant;

    public Cat(Integer id, String type, String name, Gender gender, double length,
                double weight, Condition condition, String hamilga){
        super(id,type,name,gender,length,weight, condition,hamilga);
    }

    public boolean isPregnant() {
        return isPregnant;
    }

    public void setPregnant(boolean pregnant) {
        isPregnant = pregnant;
    }

    public static void chooseAttraction() {
        System.out.print("\n--Cat--\n" +
                "Attraction by Cat\n" +
                "1. Dancing Animals\n" +
                "2. Passionate Coder\n"+
                "Please choose your preferred attractions (type the number): ");
    }
}
