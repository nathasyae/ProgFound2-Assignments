package javari.animal;

public abstract class Mammals extends Animal {
    protected boolean isPregnant;
    public static String[] listnya = {"Lion","Cat","Hamster","Whale"};

    public Mammals(Integer id, String type, String name, Gender gender, double length,
                double weight, Condition condition, String hamilga){
        super(id,type,name,gender,length,weight, condition);
        if (hamilga.equals("pregnant")) {
            this.isPregnant = true;
        } else {
            this.isPregnant = false;
        }
    }

    public boolean isPregnant() {
        return isPregnant;
    }

    public void setPregnant(boolean pregnant) {
        isPregnant = pregnant;
    }

    public boolean specficCondition(){ return !(this.isPregnant);}


}
