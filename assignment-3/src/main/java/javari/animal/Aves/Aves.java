package javari.animal.Aves;

import javari.animal.Animal;
import javari.animal.Condition;
import javari.animal.Gender;

public abstract class Aves extends Animal {
    protected boolean isLayingEggs;
    public static String[] listnya = {"Parrot","Eagle"};

    public Aves(Integer id, String type, String name, Gender gender, double length,
                double weight, Condition condition, String bertelorga) {
        super(id, type, name, gender, length, weight, condition);
        //bertelorga
        if (bertelorga.equals("laying")) {
            this.isLayingEggs = true;
        } else {
            this.isLayingEggs = false;
        }
    }


    public boolean isLayingEggs() {
        return isLayingEggs;
    }

    public void setLayingEggs(boolean layingEggs) {
        isLayingEggs = layingEggs;
    }

    public boolean specficCondition(){ return !(this.isLayingEggs);}


}
