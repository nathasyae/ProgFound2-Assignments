package javari.animal.Aves;

import javari.animal.Condition;
import javari.animal.Gender;

public class Parrot extends Aves {
    protected boolean isLayingEggs;

    public Parrot(Integer id, String type, String name, Gender gender, double length,
                double weight, Condition condition, String bertelorga) {
        super(id, type, name, gender, length, weight, condition, bertelorga);

    }

    public boolean isLayingEggs() {
        return isLayingEggs;
    }

    public void setLayingEggs(boolean layingEggs) {
        isLayingEggs = layingEggs;
    }

    public void chooseAttraction() {
        System.out.print("\n--Parrot--\n" +
                "Attraction by Parrot\n" +
                "1. Dancing Animals\n" +
                "2. Counting Masters\n"+
                "Please choose your preferred attractions (type the number): ");
    }

    @Override
    protected boolean specificCondition() {
        return false;
    }
}
