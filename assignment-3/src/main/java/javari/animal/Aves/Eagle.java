package javari.animal.Aves;

import javari.animal.Condition;
import javari.animal.Gender;

public class Eagle extends Aves {
    protected boolean isLayingEggs;

    public Eagle(Integer id, String type, String name, Gender gender, double length,
                 double weight, Condition condition, String bertelorga) {
        super(id, type, name, gender, length, weight, condition, bertelorga);

    }

    public boolean isLayingEggs() {
        return isLayingEggs;
    }

    public void setLayingEggs(boolean layingEggs) {
        isLayingEggs = layingEggs;
    }

    public static void chooseAttraction() {
        System.out.print("\n--Eagle--\n" +
                "Attraction by Eagle\n" +
                "1. Circle of Fire\n" +
                "Please choose your preferred attractions (type the number): ");
    }
}