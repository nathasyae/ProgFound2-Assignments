package javari.animal.Reptiles;

import javari.animal.Animal;
import javari.animal.Condition;
import javari.animal.Gender;

public abstract class Reptiles extends Animal {
    protected boolean isTame;
    public static String[] listnya = {"Snake"};

    public Reptiles(Integer id, String type, String name, Gender gender, double length,
                double weight, Condition condition, String jinakga){
        super(id,type,name,gender,length,weight, condition);
        if (jinakga.equals("tame")) {
            this.isTame = true;
        } else {
            this.isTame = false;
        }
    }

    public boolean isTame() {
        return isTame;
    }

    public void setTame(boolean tame) {
        isTame = tame;
    }

    public boolean specficCondition(){ return (this.isTame);}


}
