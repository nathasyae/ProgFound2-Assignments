package javari.animal.Reptiles;

import javari.animal.Condition;
import javari.animal.Gender;

public class Snake extends Reptiles {
    protected boolean isTame;

    public Snake(Integer id, String type, String name, Gender gender, double length,
                double weight, Condition condition, String jinakga){
        super(id,type,name,gender,length,weight, condition, jinakga);

    }

    public boolean isTame() {
        return isTame;
    }

    public void setTame(boolean tame) {
        isTame = tame;
    }

    public static void chooseAttraction() {
        System.out.print("\n--Snake--\n"+
                "Attraction by Snake\n"+
                "1. Dancing Animals\n"+
                "2. Passionate Coders\n"+
                "Please choose your preferred attractions (type the number): ");
    }

}
