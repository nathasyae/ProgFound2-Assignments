package javari.reader;

import javari.park.CircleOfFire;
import javari.park.CountingMasters;
import javari.park.DancingAnimals;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;

public class AnimalAttractions extends CsvReader{
    private long valid=0;
    private long invalid=0;

    public AnimalAttractions(Path file) throws IOException {
        super(file);
        count();
    }

    public void count() {
        for (int i = 0; i < lines.size(); i++) {
            String baris = lines.get(i);
            String[] barisSplit = baris.split(","); //{[0] type, [1] attraction}
            switch (barisSplit[1]) {
                case "Circle Of Fire":
                    if (Arrays.asList(CircleOfFire.ygBisa).contains(barisSplit[0])) {
                        valid += 1;
                    } else{ invalid += 1; }
                    break;
                case "Counting Masters":
                    if (Arrays.asList(CountingMasters.ygBisa).contains(barisSplit[0])) {
                        valid += 1;
                    } else{ invalid += 1; }
                    break;
                case "Dancing Animals":
                    if (Arrays.asList(DancingAnimals.ygBisa).contains(barisSplit[0])) {
                        valid += 1;
                    } else{ invalid += 1; }
                    break;
                default:
                    invalid += 1;
                    break;

            }
        }
    }

    public long getValid() {
        return valid;
    }

    public long getInvalid() {
        return invalid;
    }
}
