package javari.reader;

import javari.ProgramUtama;
import javari.animal.Animal;
import javari.animal.Aves.Aves;
import javari.animal.Aves.Eagle;
import javari.animal.Aves.Parrot;
import javari.animal.Condition;
import javari.animal.Gender;
import javari.animal.Mammals.Cat;
import javari.animal.Mammals.Hamster;
import javari.animal.Mammals.Lion;
import javari.animal.Mammals.Whale;
import javari.animal.Reptiles.Snake;
import javari.park.CircleOfFire;
import javari.park.CountingMasters;
import javari.park.DancingAnimals;
import javari.park.PassionateCoders;

import java.io.IOException;
import java.nio.file.Path;

public class AnimalRecords extends CsvReader {

    private long valid = 0;
    private long invalid = 0;
    private int id = 0;

    public AnimalRecords(Path file) throws IOException {
        super(file);
        count();
    }

    public void count() {
        for (int i = 0; i < lines.size(); i++) {
            Animal z = null;
            String in = lines.get(i);
            String[] inSplit = in.split(","); //Lion,nala,female,150,130,pregnant,healthy
            String type = inSplit[0];
            String name = inSplit[1];
            Gender gender = Gender.parseGender(inSplit[2]);
            double length = Double.parseDouble(inSplit[3]);
            double weight = Double.parseDouble(inSplit[4]);
            String special = inSplit(5);
            Condition condition = Condition.parseCondition(inSplit[6]);
            try{
                initiate(type,name,gender,length,weight,condition,special);
                valid+=1;
            } catch (Exception e) {
                invalid+=1;
            }
        }
    }

    public void initiate(String type, String name, Gender gender, double length,
                         double weight, Condition condition, String special) {
        id += 1;
        switch (type) {
            case "Whale":
                if (special != "") {
                    ProgramUtama.listAnimal.add(new Whale(id, type, name, gender, length, weight, condition, special));
                } else {
                    ProgramUtama.listAnimal.add(new Whale(id, type, name, gender, length, weight, condition));
                }
                break;
            case "Cat":
                if (special != "") {
                    ProgramUtama.listAnimal.add(new Cat(id, type, name, gender, length, weight, condition, special));
                } else {
                    ProgramUtama.listAnimal.add(new Cat(id, type, name, gender, length, weight, condition));
                }
                break;
            case "Hamster":
                if (special != "") {
                    ProgramUtama.listAnimal.add(new Hamster(id, type, name, gender, length, weight, condition, special));
                } else {
                    ProgramUtama.listAnimal.add(new Hamster(id, type, name, gender, length, weight, condition));
                }
                break;
            case "Lion":
                if (special != "") {
                    ProgramUtama.listAnimal.add(new Lion(id, type, name, gender, length, weight, condition, special));
                } else {
                    ProgramUtama.listAnimal.add(new Lion(id, type, name, gender, length, weight, condition));
                }
                break;
            case "Snake":
                if (special != "") {
                    ProgramUtama.listAnimal.add(new Snake(id, type, name, gender, length, weight, condition, special));
                } else {
                    ProgramUtama.listAnimal.add(new Snake(id, type, name, gender, length, weight, condition));
                }
                break;
            case "Eagle":
                if (special != "") {
                    ProgramUtama.listAnimal.add(new Eagle(id, type, name, gender, length, weight, condition, special));
                } else {
                    ProgramUtama.listAnimal.add(new Eagle(id, type, name, gender, length, weight, condition));
                }
                break;
            case "Parrot":
                if (special != "") {
                    ProgramUtama.listAnimal.add(new Parrot(id, type, name, gender, length, weight, condition, special));
                } else {
                    ProgramUtama.listAnimal.add(new Parrot(id, type, name, gender, length, weight, condition));
                }
                break;
            default:
                ProgramUtama.listAnimal.add(new Animal(id, type, name, gender, length, weight, condition));
                break;

        }
    }

    public void setAttraction(Animal i){
        if ( i instanceof Lion || i instanceof Whale || i instanceof Eagle ) {
            CircleOfFire.addPerformer(i);
        } else if ( i instanceof Parrot || i instanceof Snake || i instanceof Cat || i instanceof Hamster ){
            DancingAnimals.addPerformer(i);
        } else if ( i instanceof Hamster || i instanceof Whale || i instanceof Parrot ){
            CountingMasters.addPerformer(i);
        } else if ( i instanceof Hamster || i instanceof Cat || i instanceof Snake ){
            PassionateCoders.addPerformer(i);
        }
    }
}
