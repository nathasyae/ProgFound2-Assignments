package javari.reader;

import com.sun.deploy.util.ArrayUtil;
import javari.animal.Aves.Aves;
import javari.animal.Mammals;
import javari.animal.Reptiles.Reptiles;

import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

public class AnimalCategories extends CsvReader {

    private long valid = 0;
    private long invalid = 0;

    public AnimalCategories(Path file) throws IOException {
        super(file);
        count();
    }

    public void count() {
        for (int i = 0; i < lines.size(); i++) {
            String baris = lines.get(i);
            String[] barisSplit = baris.split(","); //{[0] type, [1] category,[2] section}
            switch (barisSplit[1]) {
                case "mammals":
                    if (Arrays.asList(Mammals.listnya).contains(baris[0])) {
                        if ((barisSplit[2]).equals("Explore the Mammals")) {
                            valid += 1;
                        } else{
                            invalid += 1;
                        }
                    } else {
                        invalid += 1;
                    }
                    break;
                case "reptile":
                    if (Arrays.asList(Reptiles.listnya).contains(baris[0])) {
                        if ((barisSplit[2]).equals("Reptilian Kingdom")) {
                            valid += 1;
                        } else{
                            invalid += 1;
                        }
                    } else {
                        invalid += 1;
                    }
                    break;
                case "aves":
                    if (Arrays.asList(Aves.listnya).contains(baris[0])) {
                        if ((barisSplit[2]).equals("World of Aves")) {
                            valid += 1;
                        } else{
                            invalid += 1;
                        }
                    } else {
                        invalid += 1;
                    }
                    break;
                default:
                    invalid += 1;
                    break;

            }
        }
    }


    public long getValid() {
        return valid;
    }

    public long getInvalid() {
        return invalid;
    }

}
